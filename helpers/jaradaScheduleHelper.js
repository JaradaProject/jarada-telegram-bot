const schedule = require("node-schedule")

/**
 * 
 * @param {string} startDateTime - Date time format when the schedule need to start
 * @param {string} endDateTime - Date time format when the schedule need to end
 * @param {number} interval - The interval of the scheduler, 1 = 1 second
 * @param {function} runningFunction - The function that must be running
 */
module.exports = function (startDateTime, endDateTime, interval, runningFunction) {
    const listDate = [new Date(Date.parse(startDateTime))]
    while (true) {
        const date = listDate.slice(-1).pop()
        const intervalCounter = date.getTime() + (interval * 1000)
        if (intervalCounter < new Date(Date.parse(endDateTime)).getTime())
            listDate.push(new Date(intervalCounter))
        else
            break
    }
    listDate.forEach(date => {
        schedule.scheduleJob(date, runningFunction)
    });
}