const ingressPrimeStatsToJson = require('./ingressStats')
const CustomError = require("./customErrorHandler")

module.exports = async function (stats) {
    let statJson;
    try {
        statJson = await ingressPrimeStatsToJson(stats)
    } catch (error) {
        console.log(`Stats error: ${error.message}`);
        if (error.message.includes('unknown key was added')) {
            throw new CustomError('ERROR_STAT_HANDLER', 'Stat Ingress berbeda dari yang Jarada ketahui, Tolong hubungi SkyJackerz')
        } else {
            throw new CustomError('WRONG_STATS_FORMAT', 'Format stats Ingress Salah')
        }
    }
    if (statJson['Time Span'] != 'ALL TIME')
        throw new CustomError('WRONG_STATS_TIME', 'Submit stats must from ALL TIME')
    const statKeys = Object.keys(statJson)
    statKeys.push('\n')
    Object.keys(statJson).forEach(key => {
        statKeys.push(statJson[key])
    })
    return {
        json: statJson,
        formated: statKeys.join('        ').split('\n        ').join('\n')
    }
}