function userCaller(data) {
    if (!data.username) {
        let fullName = `${data.first_name}`
        if (data.last_name)
            fullName = `${data.first_name} ${data.last_name}`
        return `[${fullName}](tg://user?id=${data.id})`
    }
    return `[${data.username}](tg://user?id=${data.id})`
}

module.exports = userCaller