const objectValidator = require('../validate')
const db = require('../models')
const CustomError = require('../helpers/customErrorHandler')

class UsersController {
    constructor(data) {
        this.data = data
    }
    async add() {
        objectValidator('addUser', this.data)
        // await db.unknowns.destroy({
        //     where: { name: this.data.name }
        // }).catch(() => { })
        await db.users.create(this.data)
            .catch(err => {
                throw new CustomError('ER_DUP_ENTRY')
            })
    }
    async update() {
        objectValidator('updateUser', this.data)
        await db.users.update(this.data, { where: { id: this.data.id } })
            .catch(err => {
                console.log(err);
                throw new CustomError('ERROR UPDATE ACCOUNT')
            })
    }
    async get() {
        let obj;
        await db.users.findOne({
            raw: true,
            where: this.data
        })
            .then(res => {
                obj = res
            })
        return obj
    }
}

module.exports = UsersController