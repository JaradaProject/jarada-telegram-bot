const axios = require('axios')
const CustomError = require('../helpers/customErrorHandler')

class ZoomController {
    constructor(data) {
        this.data = data
    }
    async get() {
        await axios({
            url: process.env.GSCRIPT_URL + '?script=getZoomLink',
            method: 'get'
        })
            .then(res => {
                this.data = res.data.response.result
            })
    }
    async update() {
        let comm = '/updatezoom'
        if (this.data.includes(`@${process.env.BOT_USERNAME}`))
            comm = `/updatezoom@${process.env.BOT_USERNAME}`
        const link = this.data.replace(`${comm} `, '')
        if (link == `${comm}`)
            throw new CustomError('UPDATE_ZOOM_LINK_EMPTY', 'Tolong masukkan link zoom ketika update')
        await axios({
            url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=updateZoomLink`,
            method: 'post',
            data: { link }
        })
            .then(res => {
                if (res.data.response.code != 200)
                    throw new CustomError('UPDATE_ZOOM_LINK_ERROR', 'Something went wrong went update zoom link')
            })
        this.data = link
    }
}

module.exports = ZoomController