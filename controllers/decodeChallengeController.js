const axios = require("axios")
const CustomError = require("../helpers/customErrorHandler")
const userCaller = require("../helpers/callUserHelpers")
const db = require("../models")
const telegram = require("../connections/botConnection").telegram

async function decode(ctx) {
    //** check ctx format */
    const text = ctx.update.message.text
    let comm = '/decode'
    if (text.includes(`@${process.env.BOT_USERNAME}`))
        comm = `${this.comm}@${process.env.BOT_USERNAME}`

    //** validate argument and get user data from telegram*/
    const code = text.replace(`${comm} `, '')
    if ((code == comm) || (code == ''))
        throw new CustomError('MISSING_ARGS', `Format Decode Challenge seharusnya:\n${comm}<spasi>PASSCODE`)

    //** get user data from ctx*/
    const id = String(ctx.update.message.from.id)
    const telegramUser = userCaller(ctx.update.message.from)

    //** delete message to perevent copying or mistaken use*/
    ctx.deleteMessage(ctx.update.message.message_id).catch(() => { })

    const event = await db.events.findOne({ raw: true, where: { status: "active" } })

    //** check user rsvp */
    await db.rsvps.findOne({ raw: true, where: { userId: id, eventId: event.id } })
        .then(async (res) => {
            if (res == null) {
                throw new CustomError("NOT_RSVP", "Kamu belum rsvp IFS, kamu tidak bisa join challenge ini")
            }
            const agent = await db.users.findOne({ raw: true, where: { id } })
            const postBody = { userId: id, agentName: agent.name, code }
            await axios({
                url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=decodeChallenge`,
                method: 'post',
                data: postBody
            }).then(result => {
                if (result.data.response && result.data.response.code == 200) {
                    const msg = `${telegramUser}\n` +
                        `Kamu berhasil solving passcode IFS Surabaya. Good Job Agent 😁👍`

                    telegram.sendMessage(id, msg, { parse_mode: 'Markdown' })
                }
                if (result.data.response && result.data.response.code != 200) {
                    console.log(result.data);
                    console.log("err: ", JSON.stringify(result.data));
                    throw new CustomError('CHALLENGE_STAT_ERR', result.data.response.result)
                }
                if (!result.data.response) {
                    console.log(result.data);
                    throw new CustomError('SERVER_ERR', 'Maaf ada gangguan, hubungi @SkyJackerz')
                }
            })
        })
}

module.exports = decode