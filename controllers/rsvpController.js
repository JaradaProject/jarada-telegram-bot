const axios = require('axios')
const cheerio = require('cheerio')
const db = require('../models')
const bot = require('../connections/botConnection')
const userCaller = require('../helpers/callUserHelpers')
const objectValidator = require('../validate')
const { telegram } = require('../connections/botConnection')
const CustomError = require('../helpers/customErrorHandler')
const ingressStatsHelper = require('../helpers/ingressStatsHelpers')
const UsersController = require('./usersController')

class RsvpController {
    constructor(data) {
        this.data = data
    }
    async getUserRsvp() {
        let obj;
        await db.rsvps.findOne({
            raw: true,
            where: this.data
        })
            .then(res => {
                obj = res
            })
        return obj
    }
    async create() {
        objectValidator('createRsvps', this.data)
        await db.rsvps.create(this.data)
    }
    async update() {
        await db.rsvps.update(this.data, { where: { userId: this.data.userId, eventId: this.data.eventId } })
    }
    async findRsvps() {
        let obj;
        await db.rsvps.findAll({
            raw: true,
            where: this.data
        }).then(res => {
            obj = res
        })
        return obj
    }
    async manual(ctx) {
        //** checking ctx text format */
        const text = ctx.update.message.text
        let comm = '/rsvp'
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `/rsvp@${process.env.BOT_USERNAME}`

        if ((text.replace(`${comm} `, '') == comm) || (text.replace(`${comm} `, '') == ''))
            throw new CustomError('MISSING_STATS', 'Format manual rsvp seharusnya: /rsvp<spasi>STATS INGRESS PRIME')
        const stats = await ingressStatsHelper(text.replace(`${comm} `, '').split('\n').map(t => t.replace(/\s\s/g, ' ')).join('\n'))

        //** checking user in database */
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        //**deleting message after get some data */
        ctx.deleteMessage(ctx.update.message.message_id)

        //**cheking user data in database */
        const user = await new UsersController({ id }).get()
        if (user && !['340704057', '273743485'].includes(user.id)) {
            const msg = `Anda tidak perlu manual RSVP, tinggal pastikan ID anda sesuai dengan di game maka akan auto rsvp.`
            throw new CustomError('RSVP_ERR', msg)
        }

        //** update or add userdata to database */
        if (!user)
            await new UsersController({
                id, name: stats.json['Agent Name'], faction: stats.json['Agent Faction']
            }).add()
        const event = await db.events.findOne({ raw: true, where: { status: 'active' } })
        await db.rsvps.findOne({ raw: true, where: { userId: id, eventId: event.id } })
            .then(res => {
                if (res) {
                    throw new CustomError('MANUAL_RSVP_ERR', 'Kamu sudah RSVP')
                }
            })
        await db.rsvps.create({
            userId: id, eventId: event.id
        }).catch(err => {
            console.log(err);
            throw new CustomError('MANUAL_RSVP_ERR', 'Error saat mencoba menyimpan ke database')
        })
        await db.unknown.destroy({ where: { name: stats.json['Agent Name'], eventId: event.id } }).catch(() => { })

        const urlEvent = `https://fevgames.net/ifs/event/?e=${event.id}`

        const msg = `${telegramUser}\n` +
            `Terpantau RSVP secara manual. Jarada ucapkan terima kasih\n` +
            `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
            `Bagi yang mau RSVP bisa ke: ${urlEvent}`

        bot.telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })


    }
    async auto(eventId) {
        const urlEvent = 'https://fevgames.net/ifs/event/?e='

        //** Get data rsvp from fev games */
        const html = await axios.get(`${urlEvent}${eventId}`)
        const $ = cheerio.load(html.data)

        const rsvpPlayers = []
        function collector(e) {
            if ($(e)[0].type == 'text') {
                const text = $(e).text().match(/([A*-z*])\w+/g)
                if (text != null)
                    rsvpPlayers.push(text[0])
            }
        }

        $('div#Res')['0'].children.forEach(element => { collector(element) });
        $('div#Enl')['0'].children.forEach(element => { collector(element) });

        //** Get user have saved before */
        const members = await db.users.findAll({ raw: true })
        const rsvps = []
        const unknown = []
        for (const i in rsvpPlayers) {
            const name = rsvpPlayers[i]
            if (members.map(user => user.name).includes(name)) {
                const userId = members[members.map(user => user.name).indexOf(name)].id;
                const addedRsvp = await db.rsvps.findOne({ raw: true, where: { userId, eventId } })
                if (!addedRsvp) {
                    await db.rsvps.create({ userId, eventId }).catch(err => { });
                    rsvps.push(userCaller({ id: userId, username: name }))
                }
            } else {
                const addedUnknown = await db.unknown.findOne({ raw: true, where: { name } })
                if (!addedUnknown) {
                    await db.unknown.create({ name, eventId }).catch(err => { });
                    unknown.push(`@${name}`)
                }
            }
        }

        let msg;
        if ((rsvps.length == 0) && (unknown.length == 0))
            msg = ''
        if ((rsvps.length > 0) && (unknown.length == 0)) {
            this.rsvps = rsvps.join(', ')
            msg = `${this.rsvps}\n` +
                `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
                `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
                `Bagi yang mau RSVP bisa ke: ${urlEvent}${eventId}`
        }
        if ((rsvps.length == 0) && (unknown.length > 0)) {
            this.unknown = unknown.join(', ')
            msg = `${this.unknown}\n` +
                `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
                `Apa bila ada yang kenal mohon bantuan untuk mengajak bergabung ke grup ini.\n` +
                `Apabila sudah ada di grup ini maka check ulang ID yang didaftarkan ke Fevgames terlebih dahulu.\n` +
                `Apabila sebelumnya belum pernah ikut IFS silahkan rsvp manual.\n` +
                `Gunakan: /rsvp<spasi>STATS INGRESS KAMU.\n` +
                `Dan bila sudah, Jarada ucapkan selamat datang.\n\n` +
                `Bagi yang mau RSVP bisa ke: ${urlEvent}${eventId}`
        }
        if ((rsvps.length > 0) && (unknown.length > 0)) {
            this.rsvps = rsvps.join(', ')
            this.unknown = unknown.join('\n')
            msg = `Halo ` + `${this.rsvps}\n` +
                `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
                `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
                `Dan buat,\n` + `${this.unknown}\n` +
                `Juga terpantau sudah RSVP di Fevgames` +
                `Apa bila ada yang kenal mohon bantuan untuk mengajak bergabung ke grup ini.\n` +
                `Apabila sudah ada di grup ini maka check ulang ID yang didaftarkan ke Fevgames terlebih dahulu.\n` +
                `Apabila sebelumnya belum pernah ikut IFS silahkan rsvp manual.\n` +
                `Gunakan: /rsvp<spasi>STATS INGRESS KAMU.\n` +
                `Dan bila sudah, Jarada ucapkan selamat datang.\n\n` +
                `Bagi yang mau RSVP bisa ke: ${urlEvent}${eventId}`
        }

        if (msg == '')
            return
        bot.telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
    }
    async notificationRsvpEndStat() {
        //** get active event */
        const activeEvent = await db.events.findOne({
            raw: true,
            where: { status: 'active' }
        })
        //** colecting user with null value on endStat */
        const listUserNullEndStat = await db.rsvps.findAll({
            raw: true,
            where: { eventId: activeEvent.id, startStat: true, endStat: false }
        }).then(async (result) => {
            const listUser = Promise.all(result.map(user => {
                return Promise.resolve(
                    db.users.findOne({
                        raw: true,
                        where: { id: user.userId }
                    })
                )
            }));
            return (await listUser).map(user => userCaller({ id: user.id, username: user.name })).join(', ')
        })
        if (listUserNullEndStat.length) {
            const msg = `Hayo lo... 😏\nJangan lupa untuk submit stat akhir\n${listUserNullEndStat}\n😉`
            telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
        }
        return
    }
    async notificationRsvpStartStat() {
        //** get active event */
        const activeEvent = await db.events.findOne({
            raw: true,
            where: { status: 'active' }
        })
        //** colecting user with null value on endStat */
        const listUserNullStartStat = await db.rsvps.findAll({
            raw: true,
            where: { eventId: activeEvent.id, startStat: false }
        }).then(async (result) => {
            const listUser = Promise.all(result.map(user => {
                return Promise.resolve(
                    db.users.findOne({
                        raw: true,
                        where: { id: user.userId }
                    })
                )
            }));
            return (await listUser).map(user => userCaller({ id: user.id, username: user.name })).join(', ')
        })
        if (listUserNullStartStat.length) {
            const msg = `Halo ${listUserNullStartStat}.\n` +
                `Stat awal sudah di buka loh, karena sebelumnya sudah RSVP di event ini ` +
                `maka teman-teman jangan lupa untuk submit stat awal.\n` +
                `Kamu bisa submit stat dengan cara:\n/statawal<spasi>STATS INGRESS PRIME`

            telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
        }
        return
    }
}

module.exports = RsvpController