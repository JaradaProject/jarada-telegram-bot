const axios = require('axios')
const cheerio = require('cheerio')
const bot = require('../connections/botConnection')
const userCaller = require('../helpers/callUserHelpers')
const db = require('../models')

module.exports = {
    fevgamesChecker: async function (eventId) {
        const eventUrl = `https://fevgames.net/ifs/event/?e=${eventId}`
        const html = await axios.get(eventUrl)
        const $ = cheerio.load(html.data)

        const rsvpPlayers = []
        function collector(e) {
            if ($(e)[0].type == 'text') {
                const text = $(e).text().match(/([A*-z*])\w+/g)
                if (text != null) {
                    rsvpPlayers.push(text[0])
                }
            }
        }

        $('div#Res')['0'].children.forEach(element => { collector(element) });
        $('div#Enl')['0'].children.forEach(element => { collector(element) });

        if (rsvpPlayers.length != 0) {
            return rsvpPlayers
        }
        return false
    },
    autoRsvps: async function (eventId) {
        const players = await this.fevgamesChecker(eventId)
        const rsvps = []
        const unknown = []
        if (players) {
            const users = await db.users.findAll({ raw: true })

            players.forEach(async (player) => {
                //** check user data in database */
                const knownAgents = users.map(user => user.name)
                if (knownAgents.includes(player)) {
                    const userId = users[knownAgents.indexOf(player)].id
                    await db.rsvps.create({ userId, eventId })
                        .then(() => {
                            rsvps.push(userCaller({ id: userId, username: player }))
                        })
                        .catch(() => { })
                }
                await db.rsvps.create({ name: player, eventId })
                    .then(() => {
                        unknown.push(`@${player}`)
                    })
                    .catch(() => { })
            });
        }
        let msg;
        if ((rsvps.length == 0) && (unknown.length == 0))
            msg = ''
        if ((rsvps.length > 0) && (unknown.length == 0)) {
            this.rsvps = rsvps.join(', ')
            msg = `${this.rsvps}\n` +
                `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
                `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
                `Bagi yang mau RSVP bisa ke: ${urlEvent}${eventId}`
        }
        if ((rsvps.length == 0) && (unknown.length > 0)) {
            this.unknown = unknown.join(', ')
            msg = `${this.unknown}\n` +
                `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
                `Apa bila ada yang kenal mohon bantuan untuk mengajak bergabung ke grup ini.\n` +
                `Apabila sudah ada di grup ini maka check ulang ID yang didaftarkan ke Fevgames terlebih dahulu.\n` +
                `Apabila sebelumnya belum pernah ikut IFS silahkan rsvp manual.\n` +
                `Gunakan: /rsvp<spasi>STATS INGRESS KAMU.\n` +
                `Dan bila sudah, Jarada ucapkan selamat datang.\n\n` +
                `Bagi yang mau RSVP bisa ke: ${urlEvent}${eventId}`
        }
        if ((rsvps.length > 0) && (unknown.length > 0)) {
            this.rsvps = rsvps.join(', ')
            this.unknown = unknown.join('\n')
            msg = `Halo ` + `${this.rsvps}\n` +
                `Terpantau RSVP di Fevgames. Jarada ucapkan terima kasih\n` +
                `Nanti saat event dimulai, Jarada bakal ingetin untuk update stat.\n\n` +
                `Dan buat,\n` + `${this.unknown}\n` +
                `Juga terpantau sudah RSVP di Fevgames` +
                `Apa bila ada yang kenal mohon bantuan untuk mengajak bergabung ke grup ini.\n` +
                `Apabila sudah ada di grup ini maka check ulang ID yang didaftarkan ke Fevgames terlebih dahulu.\n` +
                `Apabila sebelumnya belum pernah ikut IFS silahkan rsvp manual.\n` +
                `Gunakan: /rsvp<spasi>STATS INGRESS KAMU.\n` +
                `Dan bila sudah, Jarada ucapkan selamat datang.\n\n` +
                `Bagi yang mau RSVP bisa ke: ${urlEvent}${eventId}`
        }

        if (msg == '')
            return
        bot.telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
    },
    autoUnRspvs: async function (eventId) {
        const players = await this.fevgamesChecker(eventId)
            .catch(err => {
                console.log("fevgamesChecker err", JSON.stringify(err));
                console.log("auto unrsvps canceled");
                return
            })
        let unRsvp = []
        const rsvps = await db.users.findAll({
            raw: true,
            include: {
                model: db.rsvps,
                where: { eventId }
            }
        })
        const unknown = await db.unknown.findAll({ raw: true, where: { eventId } })
        rsvps.forEach(rsvp => {
            if (['340704057', '273743485'].includes(rsvp.id)) {
                return
            }
            if (!players.includes(rsvp.name)) {
                unRsvp.push({
                    id: rsvp.id,
                    name: rsvp.name
                })
            }
        });
        unknown.forEach(u => {
            if (!players.includes(u.name)) {
                unRsvp.push({
                    id: null,
                    name: u.name
                })
            }
        });
        const unRsvpPlayer = unRsvp.map(async (player) => {
            if (player.id) {
                await db.rsvps.destroy({ where: { userId: player.id, eventId } })
                return userCaller({ id: player.id, username: player.name })
            } else {
                await db.unknown.destroy({ where: { name: player.name, eventId } })
                return `@${player.name}`
            }
        })
        Promise.all(unRsvpPlayer).then(listPlayer => {
            console.log("unrsvp player: ", listPlayer);
            if (listPlayer.length == 0) {
                return
            }
            const msg = `Agent: ${listPlayer.join(', ')}\n` +
                `mengundurkan diri dari event kali ini. Sampai jumpa di event berikutnya.`
            bot.telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
        })
    }
}