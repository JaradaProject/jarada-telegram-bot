const userCaller = require("../helpers/callUserHelpers")
const CustomError = require("../helpers/customErrorHandler")
const ingressStatsHelpers = require("../helpers/ingressStatsHelpers")
const db = require("../models")
const axios = require('axios')
const telegram = require('../connections/botConnection').telegram

const config = {
    wayfarer: {
        join: {
            comm: "/joinwayfarer",
            challengeTitle: "wayfarer challenge",
            challengeSheetName: "OPR",
            ingressPointStatName: "OPR Agreements"
        },
        joinVerification: {
            comm: "/joinwayfarer",
            challengeTitle: "wayfarer challenge",
            challengeSheetName: "OPR"
        },
        update: {
            comm: "/updatewayfarer",
            challengeTitle: "wayfarer challenge",
            challengeSheetName: "OPR",
            ingressPointStatName: "OPR Agreements"
        },
        updateVerification: {
            comm: "/updatewayfarer",
            challengeTitle: "wayfarer challenge",
            challengeSheetName: "OPR"
        }
    },
    glyph: {
        join: {
            comm: "/joinglyph",
            challengeTitle: "glyph challenge",
            challengeSheetName: "GLYPH",
            ingressPointStatName: "Glyph Hack Points"
        },
        joinVerification: {
            comm: "/joinglyph",
            challengeTitle: "glyph challenge",
            challengeSheetName: "GLYPH"
        },
        update: {
            comm: "/updateglyph",
            challengeTitle: "glyph challenge",
            challengeSheetName: "GLYPH",
            ingressPointStatName: "Glyph Hack Points"
        },
        updateVerification: {
            comm: "/updateglyph",
            challengeTitle: "glyph challenge",
            challengeSheetName: "GLYPH"
        }
    }
}

const controllers = {
    setProp: function (obj) {
        globalThis.comm = obj.comm
        globalThis.challengeTitle = obj.challengeTitle
        globalThis.challengeSheetName = obj.challengeSheetName
        globalThis.ingressPointStatName = obj.ingressPointStatName
    },
    join: async function (ctx) {
        //** check ctx format */
        const text = ctx.update.message.text
        let comm = this.comm
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `${this.comm}@${process.env.BOT_USERNAME}`

        //** validate argument and get user data from telegram*/
        if ((text.replace(`${comm} `, '') == comm) || (text.replace(`${comm} `, '') == ''))
            throw new CustomError('MISSING_STATS', `Format join ${this.challengeTitle} seharusnya:\n${comm}<spasi>STATS INGRESS PRIME`)

        //** get user data from ctx*/
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        //** delete message to perevent copying or mistaken use*/
        ctx.deleteMessage(ctx.update.message.message_id).catch(() => { })

        //** validate ingress stat */
        const stats = await ingressStatsHelpers(text.replace(`${comm} `, '').split('\n').map(t => t.replace(/\s\s/g, ' ')).join('\n'))
        const statsDateTime = new Date(Date.parse(`${stats.json['Date (yyyy-mm-dd)']} ${stats.json['Time (hh:mm:ss)']}`))
        const { minDate } = await db.dateConfs.findOne({
            raw: true,
            where: { comm: this.comm }
        })
        if (statsDateTime.getTime() < new Date(minDate).getTime())
            throw new CustomError('ER_OLDER_STAT', `Stat Ingress yang diperbolehkan untuk join ${this.challengeTitle} adalah stat yang dicopy setelah tanggal ${new Date(minDate).toLocaleString('id-ID').replace(/\./g, ':')}`)

        //** check user data in jarada databases */
        await db.users.create({
            id,
            name: stats.json['Agent Name'],
            faction: stats.json['Agent Faction']
        }).catch(async (err) => {
            if (err.original.sqlMessage.includes('Duplicate entry')) {
                const user = await db.users.findOne({ raw: true, where: { id } })
                if (user.name != stats.json['Agent Name']) {
                    throw new CustomError('ERR_DIFFERENT_NAME', 'Apakah kamu pernah ganti ID? hubungi @SkyJackerz')
                }
                if (user.faction != null && user.faction != stats.json['Agent Faction']) {
                    throw new CustomError('ERR_DIFFERENT_NAME', 'Apakah kamu pernah ganti Faction? hubungi @SkyJackerz')
                }
                if (user.faction == null) {
                    await db.users.update(
                        {
                            id,
                            name: stats.json['Agent Name'],
                            faction: stats.json['Agent Faction']
                        },
                        { where: { id } }
                    )
                }
            }
        })

        //** check if user not rsvp yet */
        const event = await db.events.findOne({
            raw: true,
            where: { status: 'active' }
        })
        const isUserRsvps = await db.rsvps.findOne({
            raw: true,
            where: { userId: id, eventId: event.id }
        })
        if (!isUserRsvps) {
            const msg = `Kamu belum masuk list RSVP.\n` +
                `Pastikan sudah RSVP di Fevgames dan ID Kamu benar.\n` +
                `Bila sudah tunggu 5 menit berikutnya atau sampai ID dipanggil Jarada.\n` +
                `Kemudian ulangi untuk ${comm}.\n` +
                `Bila masih error untuk join maka hubungi @SkyJackerz.`
            throw new CustomError('USER_NOT_RSVP', msg)
        }

        //** try send data to spreadsheet */
        const postBody = {
            challengeName: this.challengeSheetName,
            userId: id,
            agentName: stats.json['Agent Name'],
            startPoint: stats.json[this.ingressPointStatName]
        }

        await axios({
            url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=joinChallenge`,
            method: 'post',
            data: postBody
        }).then(result => {
            if (result.data.response && result.data.response.code == 200) {
                const returnedStat = []
                Object.keys(stats.json).forEach(e => {
                    if (['Agent Name', 'Level', this.ingressPointStatName].includes(e))
                        returnedStat.push(`${e}:\t${stats.json[e]}`)
                });

                const msg = `${telegramUser}\n` +
                    `Kamu berhasil join ${this.challengeTitle} dengan rincian sebagai berikut:\n` +
                    `${returnedStat.join('\n')}\n\n` +
                    `Ayo semangat untuk meningkatkan point kamu.\n` +
                    `Dan jangan lupa langsung segera upload screenshoot scanner untuk verifikasi. 😁👍`

                telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
            }
            if (result.data.response && result.data.response.code != 200) {
                throw new CustomError('CHALLENGE_STAT_ERR', result.data.response.result)
            }
            if (!result.data.response) {
                console.log(result.data);
                throw new CustomError('SERVER_ERR', 'Maaf ada gangguan, hubungi @SkyJackerz')
            }
        })
    },
    joinVerification: async function (ctx) {
        //** check ctx format */
        const text = ctx.update.message.caption
        let comm = this.comm
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `${this.comm}@${process.env.BOT_USERNAME}`

        //** validate argument and get user data from telegram*/
        if (text.replace(`${comm} `, '') != comm)
            throw new CustomError('ADDITIONAL_PROP', `Format verifikasi ${this.challengeTitle} seharusnya:\n${this.comm}`)

        //** get user data from ctx*/
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        const imageId = (ctx.update.message.document != undefined) ? ctx.update.message.document.file_id : ctx.update.message.photo.pop().file_id;

        //** delete message to perevent copying or mistaken use*/
        ctx.deleteMessage(ctx.update.message.message_id)

        //** get photo url */
        await ctx.telegram.getFileLink(imageId).then(async (image) => {
            await axios({
                url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=joinChallengeVerification`,
                method: 'post',
                data: { challengeName: this.challengeSheetName, userId: id, image }
            }).then(result => {
                if (result.data.response && result.data.response.code == 200) {
                    const msg = `${telegramUser}\n` +
                        `Kamu berhasil verifikasi ${this.challengeTitle}\n` +
                        `Ayo semangat untuk meningkatkan point kamu. 😁👍`

                    telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
                }
                if (result.data.response && result.data.response.code != 200) {
                    throw new CustomError('CHALLENGE_VERIF_ERR', result.data.response.result)
                }
                if (!result.data.response) {
                    console.log(result.data);
                    throw new CustomError('SERVER_ERR', 'Maaf ada gangguan, hubungi @SkyJackerz')
                }
            })
        })

    },
    update: async function (ctx) {
        //** check ctx format */
        const text = ctx.update.message.text
        let comm = this.comm
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `${this.comm}@${process.env.BOT_USERNAME}`

        //** validate argument and get user data from telegram*/
        if ((text.replace(`${comm} `, '') == comm) || (text.replace(`${comm} `, '') == ''))
            throw new CustomError('MISSING_STATS', `Format update ${this.challengeTitle} seharusnya:\n${this.comm}<spasi>STATS INGRESS PRIME`)

        //** get user data from ctx*/
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        //** delete message to perevent copying or mistaken use*/
        ctx.deleteMessage(ctx.update.message.message_id)

        //** validate ingress stat */
        const stats = await ingressStatsHelpers(text.replace(`${comm} `, '').split('\n').map(t => t.replace(/\s\s/g, ' ')).join('\n'))
        const statsDateTime = new Date(Date.parse(`${stats.json['Date (yyyy-mm-dd)']} ${stats.json['Time (hh:mm:ss)']}`))
        const { minDate } = await db.dateConfs.findOne({
            raw: true,
            where: { comm: this.comm }
        })
        if (statsDateTime.getTime() < new Date(minDate).getTime())
            throw new CustomError('ER_OLDER_STAT', `Stat Ingress yang diperbolehkan untuk update ${this.challengeTitle} adalah stat yang dicopy setelah tanggal ${new Date(minDate).toLocaleString('id-ID').replace(/\./g, ':')}`)

        //** try send data to spreadsheet */
        const postBody = {
            challengeName: this.challengeSheetName,
            userId: id,
            agentName: stats.json['Agent Name'],
            endPoint: stats.json[this.ingressPointStatName]
        }

        await axios({
            url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=updateChallenge`,
            method: 'post',
            data: postBody
        }).then(result => {
            if (result.data.response && result.data.response.code == 200) {
                const returnedStat = []
                Object.keys(stats.json).forEach(e => {
                    if (['Agent Name', 'Level'].includes(e))
                        returnedStat.push(`${e}:\t${stats.json[e]}`)
                    if (e == this.ingressPointStatName)
                        returnedStat.push(`${e}:\t${stats.json[e]} (${result.data.response.result})`)
                });

                const msg = `${telegramUser}\n` +
                    `Kamu berhasil update ${this.challengeTitle} dengan rincian sebagai berikut:\n` +
                    `${returnedStat.join('\n')}\n\n` +
                    `Ayo semangat untuk meningkatkan point kamu.\n` +
                    `Dan jangan lupa langsung segera upload screenshoot scanner untuk verifikasi. 😁👍`

                telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
            }
            if (result.data.response && result.data.response.code != 200) {
                throw new CustomError('CHALLENGE_STAT_ERR', result.data.response.result)
            }
            if (!result.data.response) {
                console.log(result.data);
                throw new CustomError('SERVER_ERR', 'Maaf ada gangguan, hubungi @SkyJackerz')
            }
        })
    },
    updateVerification: async function (ctx) {
        //** check ctx format */
        const text = ctx.update.message.caption
        let comm = this.comm
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `${this.comm}@${process.env.BOT_USERNAME}`

        //** validate argument and get user data from telegram*/
        if (text.replace(`${comm} `, '') != comm)
            throw new CustomError('ADDITIONAL_PROP', `Format verifikasi update ${this.challengeTitle} seharusnya: /updatewayfarer`)

        //** get user data from ctx*/
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        const imageId = (ctx.update.message.document != undefined) ? ctx.update.message.document.file_id : ctx.update.message.photo.pop().file_id;

        //** delete message to perevent copying or mistaken use*/
        ctx.deleteMessage(ctx.update.message.message_id)

        //** get photo url */
        await ctx.telegram.getFileLink(imageId).then(async (image) => {
            await axios({
                url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=updateChallengeVerification`,
                method: 'post',
                data: { challengeName: this.challengeSheetName, userId: id, image }
            }).then(result => {
                if (result.data.response && result.data.response.code == 200) {
                    const msg = `${telegramUser}\n` +
                        `Kamu berhasil update verifikasi ${this.challengeTitle}\n` +
                        `Ayo semangat untuk meningkatkan point kamu. 😁👍`

                    telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
                }
                if (result.data.response && result.data.response.code != 200) {
                    throw new CustomError('CHALLENGE_VERIF_ERR', result.data.response.result)
                }
                if (!result.data.response) {
                    console.log(result.data);
                    throw new CustomError('SERVER_ERR', 'Maaf ada gangguan, hubungi @SkyJackerz')
                }
            })
        })
    }
}

module.exports = {
    utils: function (challenge, status, isVerification) {
        if (isVerification) {
            controllers.setProp(config[challenge][`${status}Verification`])
            return controllers[`${status}Verification`]
        } else {
            controllers.setProp(config[challenge][status])
            return controllers[status]
        }
    }
}