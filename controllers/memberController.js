const CustomError = require("../helpers/customErrorHandler")
const ingressStatsToJson = require('ingress-prime-stats-to-json')
const db = require('../models')
const userCaller = require("../helpers/callUserHelpers")
const telegram = require('../connections/botConnection').telegram
const UsersController = require("./usersController")

class MemberController {
    constructor(data) {
        this.data = data
    }
    async add(ctx) {
        //** Catch data from ctx */
        let comm = '/newmember'
        if (ctx.update.message.text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `/newmember@${process.env.BOT_USERNAME}`

        const arg = ctx.update.message.text.replace(`${comm} `, '')
        const id = String(ctx.update.message.from.id)
        const user = userCaller(ctx.update.message.from)

        ctx.deleteMessage(ctx.update.message.message_id)

        if (arg == comm)
            throw new CustomError('ER_ARGS_FALSE_FORMAT')
        const stats = ingressStatsToJson(arg)

        const isNameTaken = await new UsersController({ name: stats.json['Agent Name'] }).get()
        if (isNameTaken)
            throw new CustomError(
                'ER_NAME_TAKEN',
                `ID Ingress sudah dipergunakan oleh orang lain.\n` +
                `Kamu bisa saja salah copy stat.\n` +
                `Untuk memastikan bisa hubungi 👉 SkyJackerz`
            )

        await new UsersController({
            id,
            name: stats.json['Agent Name'],
            faction: stats.json['Agent Faction']
        }).add().catch(() => {
            throw new CustomError('ER_DUP_ENTRY')
        })

        // const isUser = await db.users.findOne({ raw: true, where: { id } })
        // if (isUser.name == stats['Agent Name'])
        //     throw new CustomError("USER_EXIST", `Jarada sudah mengenali kamu sebagai ${isUser.name}.`)
        // if (isUser)
        //     throw new CustomError("USER_EXIST", `Data mu tersimpan sebagai ${isUser.name}.\nApakah kamu ingin mengganti ID sebagai ${stats['Agent Name']}? gunakan command /updatemember`)

        // const isName = await db.users.findOne({ raw: true, where: { name: stats['Agent Name'] } })
        // if (isName)
        //     throw new CustomError("NAME_EXIST", `IGN ${stats['Agent Name']}, sudah dipakai oleh user lain pastikan stat yang kamu paste benar. Jika menurutmu itu adalah ID kamu, silahkan hubungi POC IFS Jawa Timur`)

        // await db.users.create({ id, name: stats['Agent Name'] })

        const msg = `${user}\nJarada berhasil mengenalimu sebagai ${stats['Agent Name']}`
        telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
    }
}

module.exports = MemberController