const axios = require('axios');
const userCaller = require('../helpers/callUserHelpers');
const CustomError = require("../helpers/customErrorHandler");
const ingressStatsHelper = require('../helpers/ingressStatsHelpers');
const EventController = require('./eventController');
const RsvpController = require('./rsvpController');
const UsersController = require('./usersController');
const telegram = require('../connections/botConnection').telegram;
const db = require('../models')

module.exports = class ScoringController {
    constructor(data) {
        this.data = data
    }
    async submitStat(ctx) {
        //** checking ctx text format */
        const text = ctx.update.message.text
        let comm = '/statawal'
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `/statawal@${process.env.BOT_USERNAME}`

        if ((text.replace(`${comm} `, '') == comm) || (text.replace(`${comm} `, '') == ''))
            throw new CustomError('MISSING_STATS', 'Format submit stat awal seharusnya: /statawal<spasi>STATS INGRESS PRIME')

        const stats = await ingressStatsHelper(text.replace(`${comm} `, '').split('\n').map(t => t.replace(/\s\s/g, ' ')).join('\n'))

        //** checking user in database */
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        //**deleting message after get some data */
        ctx.deleteMessage(ctx.update.message.message_id)

        //**cheking user data in database */
        const user = await new UsersController({ id }).get()

        //** check if player wrong input other player stat */
        if (user && (user.name != stats.json['Agent Name']))
            throw new CustomError('ER_DIFF_USERNAME',
                `Anda sebelumnya dikenal sebagai ${user.name}, ` +
                `tetapi mencoba submit stat awal sebagai ${stats.json['Agent Name']}\n\n` +
                `Bila Anda sebelumnya telah mengganti nickname Ingress Anda, ` +
                `silahkan hubungi [SkyJackerz](tg://user?id=${process.env.MASTER_ID}).`
            )
        //** chek if player try input older stat */
        const statsDateTime = new Date(Date.parse(`${stats.json['Date (yyyy-mm-dd)']} ${stats.json['Time (hh:mm:ss)']}`))
        const { minDate } = await db.dateConfs.findOne({
            raw: true,
            where: { comm: '/statawal' }
        })
        if (statsDateTime.getTime() < new Date(minDate).getTime())
            throw new CustomError('ER_OLDER_STAT', `Stat Ingress yang diperbolehkan untuk stat awal adalah stat yang dicopy setelah tanggal ${new Date(minDate).toLocaleString('id-ID').replace(/\./g, ':')}`)

        //** update or add userdata to database */
        if (!user)
            await new UsersController({
                id, name: stats.json['Agent Name'], faction: stats.json['Agent Faction']
            }).add()

        if (user && (user.faction == null))
            await new UsersController({
                id, faction: stats.json['Agent Faction']
            }).update()

        //** get active event */
        const event = await new EventController().activeEvent()

        //** check if user have rsvp */
        const getUserRsvp = await new RsvpController({
            userId: id, eventId: event.id
        }).getUserRsvp()

        if (getUserRsvp && getUserRsvp.recordStatRow)
            throw new CustomError('ER_DATA_RECORDED', `Stat awal kamu sudah disimpan sebelumnya pada:\n${getUserRsvp.updatedAt.toLocaleString('id-ID').replace(/\./g, ':')}`)

        //** sending stats data to spreadsheets */
        let row;
        await axios({
            url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=submitStats`,
            method: 'post',
            data: {
                userId: id,
                stats: stats.formated
            }
        })
            .then(res => {
                row = parseInt(res.data.response.result.row)
            })

        if (!getUserRsvp)
            await new RsvpController({
                userId: id, eventId: event.id, recordStatRow: row, startStat: true
            }).create()
        await new RsvpController({
            userId: id, eventId: event.id, recordStatRow: row, startStat: true
        }).update()

        const returnedStat = []
        Object.keys(stats.json).forEach(e => {
            if (['Agent Name', 'Level', 'Lifetime AP', 'XM Recharged'].includes(e))
                returnedStat.push(`${e}:\t${stats.json[e]}`)
        });

        const msg = `${telegramUser}\n` +
            `Stat Awal berhasil disimpan dengan rincian sebagai berikut:\n` +
            `${returnedStat.join('\n')}\n\n` +
            `Jangan lupa! Untuk ikut sesi zoom. 😁👍`

        telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
    }
    async updateStat(ctx) {
        //** checking ctx text format */
        const text = ctx.update.message.text
        let comm = '/statakhir'
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `/statakhir@${process.env.BOT_USERNAME}`

        if ((text.replace(`${comm} `, '') == comm) || (text.replace(`${comm} `, '') == ''))
            throw new CustomError('MISSING_STATS', 'Format submit stat akhir seharusnya: /statakhir<spasi>STATS INGRESS PRIME')

        const stats = await ingressStatsHelper(text.replace(`${comm} `, '').split('\n').map(t => t.replace(/\s\s/g, ' ')).join('\n'))

        //** checking user in database */
        const id = String(ctx.update.message.from.id)
        const telegramUser = userCaller(ctx.update.message.from)

        //**deleting message after get some data */
        ctx.deleteMessage(ctx.update.message.message_id)

        //** checking between user information  and stat*/
        const user = await new UsersController({ id }).get()

        if (!user)
            throw new CustomError('DATA_RECORDED_NOT_FOUND', `Kamu belum melakukan submit stat awal.\nGunakan command /statawal<spasi>STATS INGRESS PRIME\n`)

        //** check if player wrong input other player stat */
        if (user && (user.name != stats.json['Agent Name']))
            throw new CustomError('ER_DIFF_USERNAME',
                `Anda sebelumnya dikenal sebagai ${user.name}, ` +
                `tetapi mencoba submit stat akhir sebagai ${stats.json['Agent Name']}\n\n` +
                `Bila Anda sebelumnya telah mengganti nickname Ingress Anda, ` +
                `silahkan hubungi [SkyJackerz](tg://user?id=${process.env.MASTER_ID}).`
            )

        //** chek if player try input older stat */
        const statsDateTime = new Date(Date.parse(`${stats.json['Date (yyyy-mm-dd)']} ${stats.json['Time (hh:mm:ss)']}`))
        const { minDate } = await db.dateConfs.findOne({
            raw: true,
            where: { comm: '/statakhir' }
        })
        if (statsDateTime.getTime() < new Date(minDate).getTime())
            throw new CustomError('ER_OLDER_STAT', `Stat Ingress yang diperbolehkan untuk submit stat akhir adalah stat yang dicopy setelah tanggal ${new Date(minDate).toLocaleString('id-ID').replace(/\./g, ':')}`)

        //** get active event */
        const event = await new EventController().activeEvent()

        //** check if user have rsvp */
        const getUserRsvp = await new RsvpController({
            userId: id, eventId: event.id
        }).getUserRsvp()

        if (!getUserRsvp)
            throw new CustomError('DATA_RECORDED_NOT_FOUND', `Kamu belum melakukan submit stat awal.\nGunakan command /statawal<spasi>STATS INGRESS PRIME\n`)

        if (getUserRsvp && !getUserRsvp.recordStatRow)
            throw new CustomError('DATA_RECORDED_NOT_FOUND', `Kamu belum melakukan submit stat awal.\nGunakan command /statawal<spasi>STATS INGRESS PRIME\n`)

        await new RsvpController({
            userId: id, eventId: event.id, endStat: true
        }).update()

        const recordStatRow = getUserRsvp.recordStatRow

        const returnedStat = []
        let result;
        await axios({
            url: process.env.GSCRIPT_URL + `?mode=${process.env.NODE_ENV}&script=updateStats`,
            method: 'post',
            data: {
                userId: id,
                stats: stats.formated,
                recordStatRow
            }
        })
            .then(res => {
                result = res.data.response.result
            })
        Object.keys(stats.json).forEach(e => {
            if (e == 'Agent Name')
                returnedStat.push(`${e}:\t${stats.json[e]}`)
            if (['Level', 'Lifetime AP', 'XM Recharged'].includes(e))
                returnedStat.push(`${e}:\t${stats.json[e]} (+ ${result[e]})`)
        });

        const msg = `${telegramUser}\n` +
            `Stat Akhir berhasil disimpan dengan rincian sebagai berikut:\n` +
            `${returnedStat.join('\n')}\n\n` +
            `Pastikan sudah ikut sesi zoom dan pantau terus update informasi di grup. 😁👍`

        telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
    }
}