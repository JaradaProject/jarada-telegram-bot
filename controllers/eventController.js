const getArgs = require('../helpers/getArgumentsHelpers')
const db = require('../models')
const axios = require("axios")
const cheerio = require("cheerio")
const CustomError = require('../helpers/customErrorHandler')

class EventController {
    constructor(ctx) {
        this.ctx = ctx
    }
    async add() {
        const text = this.ctx.update.message.text
        let comm = `/addevent`
        if (text.includes(`@${process.env.BOT_USERNAME}`))
            comm = `/addevent@${process.env.BOT_USERNAME}`

        const id = text.replace(`${comm} `, '')
        if (id == `${comm}`)
            throw new CustomError('BAD_ARGS', 'Tolong masukkan id event dengan benar')

        this.ctx.deleteMessage(this.ctx.update.message.message_id)

        await db.events.update({ status: 'inactive' }, { where: { status: 'active' } })

        const html = await axios.get(`https://fevgames.net/ifs/event/?e=${id}`)
        const $ = cheerio.load(html.data);
        if ($('h2').text() == '#IngressFS - ')
            throw new Error('EVENT_NOT_REGISTERED', "Event dengan ID yang dimaksud tidak ditemukan silahkan periksa link event yang dikirim oleh fevgame melalui email poc")
        const name = $('h2').text()
        const start = new Date(Date.parse($('span.info')[0].children[0].data.split(' ')[0]))
        const end = new Date(start.getTime() + 86340000)
        await db.events.create({
            id, name, start, end
        })
            .catch(err => {
                throw new CustomError("ERR_ENTRY_EVENT", "Kesalahan menambahkan event, dimungkinkan event sudah diinput sebelumnya")
            })
        this.name = name

    }
    async activeEvent() {
        let obj;
        await db.events.findOne({
            raw: true,
            where: { status: 'active' }
        }).then(res => {
            obj = res
        })
        return obj
    }
}


module.exports = EventController