module.exports = {
    addUser: {
        type: 'object',
        properties: {
            id: {
                type: 'string',
                minLength: 1
            },
            name: {
                type: 'string',
                minLength: 1
            },
            faction: {
                type: 'string',
                minLength: 1
            },
            email: {
                type: 'string',
                format: 'email',
                minLength: 1
            }
        },
        required: ['id', 'name'],
        additionalProperties: false
    },
    updateUser: {
        type: 'object',
        properties: {
            id: {
                type: 'string',
                minLength: 1
            },
            name: {
                type: 'string',
                minLength: 1
            },
            faction: {
                type: 'string',
                minLength: 1
            },
            email: {
                type: 'string',
                format: 'email',
                minLength: 1
            }
        },
        required: ['id'],
        additionalProperties: false
    },
    createRsvps: {
        type: 'object',
        properties: {
            id: {
                type: 'string',
                minLength: 1
            },
            userId: {
                type: 'string',
                minLength: 1
            },
            eventId: {
                type: 'string',
                minLength: 1
            },
            recordStatRow: {
                type: 'number',
                minimum: 1
            },
            startStat: {
                type: 'boolean'
            }
        },
        required: ['userId', 'eventId'],
        additionalProperties: false
    }
}