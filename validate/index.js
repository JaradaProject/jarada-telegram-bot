const Ajv = require('ajv').default
const addFormats = require('ajv-formats')
const yaml = require('js-yaml');
const fs = require('fs');
const CustomError = require('../helpers/customErrorHandler')

const ajv = new Ajv()
addFormats(ajv)
const schema = require('./schema')

module.exports = function (schema_name, body) {
    const validator = ajv.compile(schema[schema_name])
    const isValid = validator(body)
    if (!isValid) {
        throw new CustomError("ERR_VALIDATION",
            "Wrong body",
            validator.errors.map(err => `${err.dataPath} ${err.message}`).join())
    }
}

