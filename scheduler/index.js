const jaradaScheduleHelper = require('../helpers/jaradaScheduleHelper');
const objSchedules = require('./schedule')

function runScheduler() {
    const schedule = Object.keys(objSchedules)
    schedule.forEach(e => {
        jaradaScheduleHelper(
            objSchedules[e].start,
            objSchedules[e].end,
            objSchedules[e].interval,
            objSchedules[e].function
        )
    });
}

module.exports = runScheduler