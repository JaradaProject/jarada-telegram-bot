const RsvpController = require("../controllers/rsvpController");
const { telegram } = require("../connections/botConnection");
const rsvpFevgamesController = require("../controllers/rsvpFevgamesController");
const db = require("../models");

module.exports = {
    "testingScheduler" : {
        "start" : "03 Sep 2021 22:00:00",
        "end": "03 Sep 2021 22:40:00",
        "interval": 300,
        "function": async function () {
            telegram.sendMessage(process.env.IFS_GROUP_ID, `Selamat Pagi!!! ${new Date().toLocaleString()}`, { parse_mode: 'Markdown' })
        }
    },
    "autoUnRsvp": {
        "start": "01 Sep 2021 00:00:00",
        "end": "03 Sep 2021 00:00:00",
        "interval": 300,
        "function": async function () {
            console.log(`Scheduler:\t\tRunning unrsvp checker in Fevgames`);
            const event = await db.events.findOne({
                raw: true,
                where: { status: 'active' }
            })
            await rsvpFevgamesController.autoUnRspvs(event.id)
        }
    },
    "autoRsvp": {
        "start": "01 Sep 2021 00:00:00",
        "end": "03 Sep 2021 00:00:00",
        "interval": 300,
        "function": async function () {
            console.log(`Scheduler:\t\tRunning rsvp checker in Fevgames`);
            const event = await db.events.findOne({
                raw: true,
                where: { status: 'active' }
            })
            await new RsvpController().auto(event.id)
        }
    },
    "notificationStartStat": {
        "start": "04 Sep 2021 03:00:00",
        "end": "04 Sep 2021 07:30:00",
        "interval": 1800,
        "function": async function () {
            console.log(`Scheduler:\t\tRunning notification start stat to group`);
            await new RsvpController().notificationRsvpStartStat()
        }
    },
    "notificationEndStat": {
        "start": "04 Sep 2021 07:00:00",
        "end": "04 Sep 2021 10:30:00",
        "interval": 1800,
        "function": async function () {
            console.log(`Scheduler:\t\tRunning notification end stat to group`);
            await new RsvpController().notificationRsvpEndStat()
        }
    }
}