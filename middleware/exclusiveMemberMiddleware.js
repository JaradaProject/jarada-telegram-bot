const CustomError = require("../helpers/customErrorHandler")

module.exports = function (ctx, next) {
    if (!process.env.EXCLUSIVE_MEMBER.split(",").includes(String(ctx.update.message.from.id)))
        throw new CustomError('UNREPLY_ERROR')
    next()
}