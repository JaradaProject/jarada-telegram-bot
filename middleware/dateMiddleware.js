const CustomError = require("../helpers/customErrorHandler")
const db = require('../models')

module.exports = async function (ctx, next) {
    const txt = (ctx.update.message.text) ? ctx.update.message.text : ctx.update.message.caption;
    let comm = txt.split(' ')[0]
    if (comm.includes(`@${process.env.BOT_USERNAME}`))
        comm = comm.replace(`@${process.env.BOT_USERNAME}`, '')

    if (process.env.MT_STAT != 'on') {
        const eventDate = await db.dateConfs.findOne({
            raw: true,
            where: { comm: 'event' }
        })

        let dateMin = eventDate.minDate
        let dateMax = eventDate.maxDate
        const date = new Date()

        if (date.getTime() < dateMin.getTime())
            throw new CustomError('UPCOMING_EVENT', `${comm} tidak bisa digunakan, IFS akan diselenggarakan tanggal ${dateMin.toLocaleDateString('id-ID')}`)
        if (date.getTime() > dateMax.getTime())
            throw new CustomError('MISSED EVENT', `${comm} tidak bisa digunakan, IFS terakhir telah lewat dari jadwal`)

        await db.dateConfs.findOne({
            raw: true,
            where: { comm }
        })
            .then(res => {
                dateMin = res.minDate
                dateMax = res.maxDate
            })

        if (date.getTime() < dateMin.getTime())
            throw new CustomError('UPCOMING_EVENT', `${comm} tidak bisa digunakan sebelum ${dateMin.toLocaleString('id-ID')}`)
        if (date.getTime() > dateMax.getTime())
            throw new CustomError('MISSED_EVENT', `${comm} tidak bisa digunakan setelah ${dateMax.toLocaleString('id-ID')}`)
    }
    next()

}