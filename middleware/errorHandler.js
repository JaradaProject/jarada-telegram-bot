const userCaller = require("../helpers/callUserHelpers")
const telegram = require('../connections/botConnection').telegram

const errorIdentified = {
    "ER_DUP_ENTRY": "Command has been added before",
    "ER_ARGS_NO_COMMAND": `Command ini membutuhkan argument`,
    "ER_ARGS_FALSE_FORMAT": `Format argumen salah`,
    "UNDER_DEVELOPMENT": `Fitur ini masih dalam tahap pengembangan`
}

async function errorHandler(ctx, err) {
    let msg = err.message
    if (err.name == 'UNREPLY_ERROR') {
        console.log('catch unreply error: ', err)
        return
    }
    if (Object.keys(errorIdentified).includes(err.name))
        msg = errorIdentified[err.name]
    if (!err.message) {
        console.log('catch undefined error: ', err)
        msg = `Pesan error tidak terdefinisi, memanggil [Creator](tg://user?id=${process.env.MASTER_ID})`
    }
    const user = ctx.update.message.from
    const { id, type } = await ctx.getChat()
    if (type != 'private')
        return telegram.sendMessage(
            process.env.IFS_GROUP_ID,
            `⛔️ADA YANG SALAH!⛔️\n` +
            `${userCaller(user)}\n${msg}`,
            { parse_mode: 'Markdown' }
        )
    telegram.sendMessage(
        id,
        `⛔️ADA YANG SALAH!⛔️\n` +
        `${userCaller(user)}\n${msg}`,
        { parse_mode: 'Markdown' }
    )
    return
}

module.exports = errorHandler