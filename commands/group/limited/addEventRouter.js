const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const { telegram } = require('../../../connections/botConnection')
const errorHandler = require("../../../middleware/errorHandler");
const adminMiddleware = require('../../../middleware/adminMiddleware');
const EventController = require('../../../controllers/eventController');

const bot = new Composer();
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await adminMiddleware(ctx, next)

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const event = new EventController(ctx)
            await event.add()
            telegram.sendMessage(process.env.MASTER_ID, `Berhasil menambahkan event ${event.name}`)
        }
    } catch (error) {
        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(() => { })
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/addevent', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/addevent@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing