const { Extra } = require('telegraf')
const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const userCaller = require('../../../helpers/callUserHelpers')
const errorHandler = require("../../../middleware/errorHandler");
const dateMiddleware = require('../../../middleware/dateMiddleware');
const EventController = require('../../../controllers/eventController');

const bot = new Composer();
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await dateMiddleware(ctx, next)

        const msg = 'Ini link rules IFS Challenge Edisi Surabaya:\n' +
            `👉️ [KLIK DI SINI](https://gitlab.com/JaradaProject/jarada-telegram-bot/-/blob/docs/DECODE.md)` +
            '\n\n' +
            '⚠️ Jangan lupa untuk terus update info-info di group ya ☺️.'

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const user = ctx.update.message.from
            ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`, Extra.webPreview(false))
        }
    } catch (error) {
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/linkrules', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/linkrules@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing
