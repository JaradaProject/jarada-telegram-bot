const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const dateMiddleware = require("../../../middleware/dateMiddleware");
const errorHandler = require("../../../middleware/errorHandler");
const jaradaChallengeController = require('../../../controllers/jaradaChallengeController');

const bot = new Composer()
const routing = new Router(({ message }) => {
    try {
        if (message.text) {
            return { route: message.text.split(' ')[0] }
        }
        if (message.caption) {
            return { route: message.caption.split(' ')[0] }
        }
    } catch (error) { }
})

async function handler(ctx, next, challenge, status) {
    try {
        //** date middleware */
        await dateMiddleware(ctx, next)
        //** check user send text or photo */
        const response = await ctx.getChat()
        if (response.type != 'private') {
            if (ctx.update.message.caption) {
                //** join verification */
                await jaradaChallengeController.utils(challenge, status, true)(ctx)
            } else {
                await jaradaChallengeController.utils(challenge, status, false)(ctx)
            }
        }
    } catch (err) {
        console.log(err);
        if (ctx.update.message.message_id) {
            ctx.deleteMessage().catch(() => { })
        }
        bot.use(errorHandler(ctx, err))
    }
}

routing.on('/joinwayfarer', async (ctx, next) => {
    await handler(ctx, next, 'wayfarer', 'join')
})

routing.on(`/joinwayfarer@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next, 'wayfarer', 'join')
})

routing.on('/updatewayfarer', async (ctx, next) => {
    await handler(ctx, next, 'wayfarer', 'update')
})

routing.on(`/updatewayfarer@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next, 'wayfarer', 'update')
})

routing.on('/joinglyph', async (ctx, next) => {
    await handler(ctx, next, 'glyph', 'join')
})

routing.on(`/joinglyph@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next, 'glyph', 'join')
})

routing.on('/updateglyph', async (ctx, next) => {
    await handler(ctx, next, 'glyph', 'update')
})

routing.on(`/updateglyph@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next, 'glyph', 'update')
})

module.exports = routing