const { Extra } = require('telegraf')
const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const userCaller = require('../../../helpers/callUserHelpers')
const errorHandler = require("../../../middleware/errorHandler");
const dateMiddleware = require('../../../middleware/dateMiddleware');
const EventController = require('../../../controllers/eventController');

const bot = new Composer();
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await dateMiddleware(ctx, next)

        const event = new EventController()
        const activeEvent = await event.activeEvent()
        const eventUrl = `https://fevgames.net/ifs/event/?e=${activeEvent.id}`

        const msg = 'Ini link rsvpnya:\n' +
            `👉️ [KLIK DI SINI](${eventUrl})` +
            '\n\n' +
            '⚠️ Jangan lupa ketika daftar pastikan ID kamu sesuai dengan yang di scanner'

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const user = ctx.update.message.from
            ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`, Extra.webPreview(false))
        }
    } catch (error) {
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/linkrsvp', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/linkrsvp@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing