const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const { telegram } = require('../../../connections/botConnection')
const errorHandler = require("../../../middleware/errorHandler");
const exclusiveMemberMiddleware = require('../../../middleware/exclusiveMemberMiddleware');

const bot = new Composer();
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next, agent) {
    try {
        await exclusiveMemberMiddleware(ctx, next)

        const response = await ctx.getChat()
        if (response.type != 'private') {
            ctx.deleteMessage(ctx.update.message.message_id)
            const msg = `${agent} - Agent no longer in the system.`
            telegram.sendMessage(process.env.IFS_GROUP_ID, msg, { parse_mode: 'Markdown' })
        }
    } catch (error) {
        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(() => { })
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/banapip', async (ctx, next) => {
    await handler(ctx, next, '[@AfifConiSheyla](tg://user?id=273743485)')
})

routing.on(`/banapip@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next, '[@AfifConiSheyla](tg://user?id=273743485)')
})

routing.on('/banrohim', async (ctx, next) => {
    await handler(ctx, next, '[@ARplus29](tg://user?id=629290136)')
})

routing.on(`/banrohim@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next, '[@ARplus29](tg://user?id=629290136)')
})

module.exports = routing