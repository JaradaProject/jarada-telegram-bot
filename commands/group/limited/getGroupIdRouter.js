const Router = require('telegraf/router')
const adminMiddleware = require('../../../middleware/adminMiddleware')

const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    await adminMiddleware(ctx, next)
    const { id, type } = await ctx.getChat()
    console.log(id);
    if (type != 'private') {
        ctx.deleteMessage(ctx.update.message.message_id)
    }
}

routing.on('/getgroupid', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/getgroupid@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing