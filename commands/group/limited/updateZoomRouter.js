const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const { telegram } = require('../../../connections/botConnection')
const errorHandler = require("../../../middleware/errorHandler");
const ZoomController = require('../../../controllers/zoomController');
const adminMiddleware = require('../../../middleware/adminMiddleware');

const bot = new Composer();
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await adminMiddleware(ctx, next)

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const zoom = new ZoomController(ctx.update.message.text)
            ctx.deleteMessage(ctx.update.message.message_id)
            await zoom.update()
            telegram.sendMessage(process.env.MASTER_ID, `Zoom link has updated to ${zoom.data}`)
        }
    } catch (error) {
        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(() => { })
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/updatezoom', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/updatezoom@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing