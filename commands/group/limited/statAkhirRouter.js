const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const errorHandler = require("../../../middleware/errorHandler");
const ScoringController = require('../../../controllers/scoringController');
const dateMiddleware = require('../../../middleware/dateMiddleware');

const bot = new Composer()
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await dateMiddleware(ctx, next)

        const { type } = await ctx.getChat()
        if (type != 'private') {
            await new ScoringController().updateStat(ctx)
        }
    } catch (err) {
        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(() => { })
        bot.use(errorHandler(ctx, err))
    }
}

routing.on('/statakhir', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/statakhir@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing