const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const errorHandler = require("../../../middleware/errorHandler");
const dateMiddleware = require('../../../middleware/dateMiddleware');
const RsvpController = require('../../../controllers/rsvpController');

const bot = new Composer()
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await dateMiddleware(ctx, next)

        const { type } = await ctx.getChat()
        if (type != 'private') {
            //** manual rsvp controller */
            await new RsvpController().manual(ctx)
        }
    } catch (err) {
        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(() => { })
        bot.use(errorHandler(ctx, err))
    }
}

routing.on('/rsvp', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/rsvp@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing