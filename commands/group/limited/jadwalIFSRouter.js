const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const userCaller = require('../../../helpers/callUserHelpers')
const dateMiddleware = require("../../../middleware/dateMiddleware");
const errorHandler = require("../../../middleware/errorHandler");

const bot = new Composer();
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        await dateMiddleware(ctx, next)

        const msg = 'Ini jadwal IFS hari ini' +
            '\n\n' +
            '💠️ Submit Stat Awal pukul:\n👉️ 10:00 - 14:30 WIB' +
            '\n\n' +
            '💠️ Zoom Ready pukul:\n👉️ 11:00 WIB' +
            '\n\n' +
            '💠️ Sesi foto zoom pukul:\n👉️ 13:00, 14:00, dan 15:00 WIB' +
            '\n\n' +
            '💠️ Submit Stat Akhir pukul:\n👉️ 14:00 - 17:00 WIB.'

        const response = await ctx.getChat()
        if (response.type != 'private') {
            const user = ctx.update.message.from
            ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`)
        }
    } catch (error) {
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/jadwalifs', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/jadwalifs@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing