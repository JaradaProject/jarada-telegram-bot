const Router = require('telegraf/router')
const userCaller = require('../../../helpers/callUserHelpers')

const routing = new Router(({ message }) => {
    try {
        if (message.new_chat_member)
            return { route: 'new_chat_member' }
    } catch (error) { }
})

routing.on('new_chat_member', (ctx) => {
    if (ctx.updateSubTypes[0] == 'new_chat_members') {
        const newUser = ctx.update.message.new_chat_member
        let msg = `\n` +
            `Apabila butuh bantuan ku, silahkan ketik:\n/tolong\n` +
            `atau kalau mau kenal aku, silahkan ketik:\n/tentang\n` +
            `Tapi karena aku masih baru jadi bot, tolong jangan sering sering ya`
        if (!newUser.username) {
            msg = `Jangan lupa set username telegramnya ya biar aku enak manggilnya.\n\n` +
                `Apabila butuh bantuan ku, silahkan ketik:\n/tolong\n` +
                `atau kalau mau kenal aku, silahkan ketik:\n/tentang\n` +
                `Tapi karena aku masih baru jadi bot, tolong jangan sering sering ya`
        }
        ctx.replyWithMarkdown(
            `👋️ Selamat datang di group IFS Jatim,\n` +
            `Om/Tante ${userCaller(newUser)}\n` +
            `${msg}`
        )
    }
})

module.exports = routing