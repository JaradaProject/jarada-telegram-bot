const Router = require('telegraf/router')
const userCaller = require('../../../helpers/callUserHelpers')

const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    const msg = 'Aku di sini bisa:\n' +
        '/statawal ini untuk submit stats awal kamu' +
        '\n\n' +
        '/statakhir ini untuk submit stats akhir kamu' +
        '\n\n' +
        '/linkzoom ini untuk memberimu link zoom untuk sesi foto' +
        '\n\n' +
        '/jadwalifs ini untuk memberikan list jadwal ifs pada hari ini'

    const response = await ctx.getChat()
    if (response.type != 'private') {
        const user = ctx.update.message.from
        ctx.replyWithMarkdown(`${userCaller(user)}\n${msg}`)
    }
}

routing.on('/tolong', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/tolong@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing