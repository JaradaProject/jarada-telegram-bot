const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const errorHandler = require("../../../middleware/errorHandler");
const CustomError = require('../../../helpers/customErrorHandler');

const bot = new Composer()
const routing = new Router(({ message }) => {
    try { return { route: message.text.split(' ')[0] } } catch (error) { }
})

async function handler(ctx, next) {
    try {
        throw new CustomError('UNDER_DEVELOPMENT')
    } catch (error) {
        bot.use(errorHandler(ctx, error))
    }
}

routing.on('/newmember', async (ctx, next) => {
    await handler(ctx, next)
})

routing.on(`/newmember@${process.env.BOT_USERNAME}`, async (ctx, next) => {
    await handler(ctx, next)
})

module.exports = routing