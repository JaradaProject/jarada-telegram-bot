const Router = require('telegraf/router')
const userCaller = require('../../../helpers/callUserHelpers')

const routing = new Router(({ message }) => {
    try { return { route: message.text } } catch (error) { }
})

routing.on('/start', async (ctx) => {
    const response = await ctx.getChat()
    if (response.type != 'private') return
    let msg = 'aku adalah jarada salam kenal'
    if (!response.username)
        msg = `aku adalah jarada salam kenal.\n` +
            `Jangan lupa set username kamu biar aku enak manggilnya.`
    ctx.replyWithMarkdown(
        `👋️ Halo ${userCaller(response)}, ${msg}.`
    )
})

module.exports = routing