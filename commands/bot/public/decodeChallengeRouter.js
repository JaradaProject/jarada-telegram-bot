const Router = require('telegraf/router')
const Composer = require('telegraf/composer')
const dateMiddleware = require("../../../middleware/dateMiddleware");
const errorHandler = require("../../../middleware/errorHandler");
const CustomError = require('../../../helpers/customErrorHandler');
const decode = require('../../../controllers/decodeChallengeController');

const bot = new Composer()
const routing = new Router(({ message }) => {
    try {
        if (message.text) {
            return { route: message.text.split(' ')[0] }
        }
        if (message.caption) {
            return { route: message.caption.split(' ')[0] }
        }
    } catch (error) { }
})

async function handler(ctx, next) {
    try {
        //** date middleware */
        await dateMiddleware(ctx, next)

        const response = await ctx.getChat()
        if (response.type != 'private') {
            throw new CustomError("DECODE_NOT_PRIVATE", `Decode Challenge hanya bisa dikirim lewat pesan private ke @${process.env.BOT_USERNAME}\n\nlebih lanjut baca di /linkrules`)
        }
        //** decode controller */
        await decode(ctx)
    } catch (err) {
        console.log(err);
        if (ctx.update.message.message_id) {
            ctx.deleteMessage().catch(() => { })
        }
        bot.use(errorHandler(ctx, err))
    }
}

routing.on('/decode', handler)

module.exports = routing