const { telegram } = require('../connections/botConnection')

const botCommands = [
    {
        "command": "/tentang",
        "description": "Deskripsi tentang Jarada"
    },
    {
        "command": "/tolong",
        "description": "Memunculkan beberapa command untuk memerintah Jarada"
    },
    {
        "command": "/linkrsvp",
        "description": "Memunculkan link rsvp"
    },
    {
        "command": "/rsvp",
        "description": "Manual RSVP, jangan gunakan ini bila sebelumnya sudah ikut"
    },
    {
        "command": "/linkrules",
        "description": "Memunculkan link rules IFS"
    },
    // {
    //     "command": "/joinwayfarer",
    //     "description": "Untuk mengikuti wayfarer challenge"
    // },
    // {
    //     "command": "/updatewayfarer",
    //     "description": "Untuk update stats yang mengikuti wayfarer challenge"
    // },
    // {
    //     "command": "/joinglyph",
    //     "description": "Untuk mengikuti glyph challenge"
    // },
    // {
    //     "command": "/updateglyph",
    //     "description": "Untuk update stats yang mengikuti glyph challenge"
    // },
    {
        "command": "/jadwalifs",
        "description": "Memunculkan rundown kegiatan IFS"
    },
    {
        "command": "/linkzoom",
        "description": "Mumunculkan link zoom kegiatan IFS"
    },
    {
        "command": "/statawal",
        "description": "Tambahkan stat ingress kamu untuk mensubmit stat awal ketika event IFS berlangsung"
    },
    {
        "command": "/statakhir",
        "description": "Tambahkan stat ingress kamu untuk mensubmit stat akhir ketika event IFS berlangsung"
    }
]

module.exports = async function () {
    await telegram.setMyCommands(botCommands)
}