'use strict';
const { v4 } = require('uuid')

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rsvps extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.users)
      this.belongsTo(models.events)
    }
  };
  rsvps.init({
    id: {
      allowNull: false,
      defaultValue: v4,
      primaryKey: true,
      unique: true,
      type: DataTypes.STRING
    },
    userId: {
      allowNull: false,
      type: DataTypes.STRING,
      references: {
        model: "users",
        key: "id"
      }
    },
    eventId: {
      allowNull: false,
      type: DataTypes.STRING,
      references: {
        model: "events",
        key: "id"
      }
    },
    recordStatRow: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.INTEGER
    },
    startStat: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
    endStat: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN
    },
  }, {
    sequelize,
    modelName: 'rsvps',
  });
  return rsvps;
};