'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.rsvps)
    }
  };
  users.init({
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      type: DataTypes.STRING
    },
    name: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    faction: {
      allowNull: true,
      type: DataTypes.STRING
    },
    email: {
      allowNull: true,
      unique: true,
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'users',
  });
  return users;
};