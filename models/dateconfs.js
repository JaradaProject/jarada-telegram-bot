'use strict';
const {
  Model
} = require('sequelize');

const { v4 } = require('uuid')

module.exports = (sequelize, DataTypes) => {
  class dateConfs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  dateConfs.init({
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      defaultValue: v4,
      type: DataTypes.STRING
    },
    comm: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    minDate: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    },
    maxDate: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    modelName: 'dateConfs',
  });
  return dateConfs;
};