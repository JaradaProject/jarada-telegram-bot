'use strict';
const { v4 } = require('uuid')

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class unknown extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  unknown.init({
    id: {
      allowNull: false,
      defaultValue: v4,
      primaryKey: true,
      unique: true,
      type: DataTypes.STRING
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    eventId: {
      allowNull: true,
      type: DataTypes.STRING,
      references: {
        model: "events",
        key: "id"
      }
    },
    createdAt: {
      defaultValue: new Date(),
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      defaultValue: new Date(),
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'unknown',
  });
  return unknown;
};