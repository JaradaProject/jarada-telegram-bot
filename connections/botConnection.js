const { Telegraf } = require('telegraf');
const TelegrafLogger = require('telegraf-logger');

const token = (process.env.NODE_ENV == "production") ? process.env.BOT_TOKEN : process.env.BOT_TOKEN_TEST;

const bot = new Telegraf(token)

// const logger = new TelegrafLogger({
//     log: console.log,
//     format: '%ut => @%u %fn %ln (%fi): <%ust> %c',
//     contentLength: 100,
// });

// bot.use(logger.middleware());

module.exports = bot