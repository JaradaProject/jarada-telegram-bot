FROM node:14.17.1-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm i sequelize-cli -g --production
RUN npm install --production
COPY . .
CMD ["npm", "start"]
