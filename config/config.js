require('dotenv').config()
let { DB_USERNAME, DB_PASSWORD, JARADA_DATABASE, JARADA_DATABASE_TEST, DB_HOST, DB_PORT } = process.env

DB_PORT = (DB_PORT) ? DB_PORT : 3306;
const pool = {
  max: 15,
  min: 5,
  idle: 20000,
  evict: 15000,
  acquire: 30000
}

module.exports = {
  "development": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": JARADA_DATABASE_TEST,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    "logging": false,
    "connectTimeout": 15000,
    "pool": pool
  },
  "production": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": JARADA_DATABASE,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": "mysql",
    "logging": false,
    "connectTimeout": 15000,
    "pool": pool
  }
}
